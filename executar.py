import sys

from dominio import converter_entrada


def main():
    entrada = converter_entrada(file(sys.argv[1]).read())
    print '\n'.join([caso.descricao for caso in entrada])

if __name__ == '__main__':
    main()
