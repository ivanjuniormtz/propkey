import unittest

from dominio import Caso
from dominio import converter_linha
from dominio import converter_peca
from dominio import converter_entrada


class ConverterLinhaTest(unittest.TestCase):

    def test_linha_vazia(self):
        dados = "......"
        esperado = [0, 0, 0, 0, 0, 0]

        self.assertEquals(esperado, converter_linha(dados))

    def test_linha_cheia(self):
        dados = "######"
        esperado = [1, 1, 1, 1, 1, 1]

        self.assertEquals(esperado, converter_linha(dados))

    def test_linha_mista(self):
        dados = "#.###."
        esperado = [1, 0, 1, 1, 1, 0]

        self.assertEquals(esperado, converter_linha(dados))


class ConverterPecaTest(unittest.TestCase):

    def test_linhas_com_tamanhos_iguais(self):
        dados = '''#.##
###.'''
        esperado = [
            [1, 0, 1, 1],
            [1, 1, 1, 0]
        ]

        self.assertEquals(esperado, converter_peca(dados))

    def test_linhas_com_tamanhos_diferentes(self):
        dados = '''#.##
#.#.#...
###.'''
        esperado = [
            [1, 0, 1, 1],
            [1, 0, 1, 0, 1, 0, 0, 0],
            [1, 1, 1, 0]
        ]

        self.assertEquals(esperado, converter_peca(dados))


ENTRADA = \
'''4
2 4
#.##
###.
3 6
#....#
#....#
#..###
2 3
##.
.##
2 7
#.#.#.#
.#.#.#.
1 1
#
1 10
###....###
3 2
##
.#
.#
1 5
#.#.#'''


class ConverterEntradaTest(unittest.TestCase):

    def test_entrada_correta(self):
        esperado = [
            Caso(
                chave=[
                    [1, 0, 1, 1],
                    [1, 1, 1, 0]
                ],

                fechadura=[
                    [1, 0, 0, 0, 0, 1],
                    [1, 0, 0, 0, 0, 1],
                    [1, 0, 0, 1, 1, 1]
                ]
            ),

            Caso(
                chave=[
                    [1, 1, 0],
                    [0, 1, 1],
                ],

                fechadura=[
                    [1, 0, 1, 0, 1, 0, 1],
                    [0, 1, 0, 1, 0, 1, 0],
                ]
            ),

            Caso(
                chave=[
                    [1],
                ],

                fechadura=[
                    [1, 1, 1, 0, 0, 0, 0, 1, 1, 1],
                ]
            ),

            Caso(
                chave=[
                    [1, 1],
                    [0, 1],
                    [0, 1],
                ],

                fechadura=[
                    [1, 0, 1, 0, 1],
                ]
            )
        ]

        self.assertEquals(esperado, converter_entrada(ENTRADA))


class CasoTest(unittest.TestCase):

    def test_profundidade_2(self):
        caso = Caso(
            chave=[
                [1, 0, 1, 1],
                [1, 1, 1, 0]
            ],

            fechadura=[
                [1, 0, 0, 0, 0, 1],
                [1, 0, 0, 0, 0, 1],
                [1, 0, 0, 1, 1, 1]
            ]
        )

        self.assertEquals(2, caso.profundidade)

    def test_profundidade_0(self):
        caso = Caso(
            chave=[
                [1, 1, 0],
                [0, 1, 1],
            ],

            fechadura=[
                [1, 0, 1, 0, 1, 0, 1],
                [0, 1, 0, 1, 0, 1, 0],
            ]
        )

        self.assertEquals(0, caso.profundidade)

    def test_profundidade_1(self):
        caso = Caso(
            chave=[
                [1],
            ],

            fechadura=[
                [1, 1, 1, 0, 0, 0, 0, 1, 1, 1],
            ]
        )

        self.assertEquals(1, caso.profundidade)

    def test_outro_com_profundidade_2(self):
        caso = Caso(
            chave=[
                [1, 1],
                [0, 1],
                [0, 1],
            ],

            fechadura=[
                [1, 0, 1, 0, 1],
            ]
        )

        self.assertEquals(2, caso.profundidade)


main = unittest.main
if __name__ == '__main__':
    main()
