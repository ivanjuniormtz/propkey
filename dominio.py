

class Caso(object):

    def __init__(self, chave, fechadura):
        self.chave = chave
        self.fechadura = fechadura
        self._profundida_da_fechadura = len(fechadura)

    def __eq__(self, other):
        return self.chave == other.chave \
         and self.fechadura == other.fechadura

    @property
    def profundidade(self):
        profundidade = 0

        while len(self.chave):
            base_da_chave = self.chave.pop()

            try:
                topo_da_fechadura, self.fechadura = \
                 self.fechadura[0], self.fechadura[1:]

            except IndexError:
                profundidade += 1
                break

            while len(base_da_chave) < len(topo_da_fechadura):
                base_da_chave = base_da_chave[:len(topo_da_fechadura)]

                resultado = map(
                    lambda x: x[0] & x[1],
                    zip(base_da_chave, topo_da_fechadura)
                )

                if resultado == [0] * len(resultado):
                    profundidade += 1
                    break

                base_da_chave = [0] + base_da_chave

        return profundidade

    @property
    def descricao(self):
        profundidade = self.profundidade
        mensagem = "The key falls to depth %s." % (profundidade)

        if profundidade == self._profundida_da_fechadura:
            mensagem = "The key can fall through."

        return mensagem


def converter_linha(dados):
    return map(lambda i: 1 if i == '#' else 0, dados)


def converter_peca(dados):
    linhas = dados.split()
    return map(converter_linha, linhas)


def converter_entrada(dados):
    linhas = dados.split('\n')
    qtde_testes, linhas = int(linhas[0]), linhas[1:]

    if qtde_testes == 0:
        return []

    saida = []

    while len(linhas):
        dado, linhas = linhas[0], linhas[1:]

        if not dado:
            break

        qtde_de_linhas, _ = map(int, dado.strip().split(' '))
        linhas_da_chave, linhas = linhas[:qtde_de_linhas], linhas[qtde_de_linhas:]

        dado, linhas = linhas[0], linhas[1:]
        qtde_de_linhas, _ = map(int, dado.strip().split(' '))
        linhas_da_fechadura, linhas = linhas[:qtde_de_linhas], linhas[qtde_de_linhas:]

        saida.append(Caso(
            converter_peca('\n'.join(linhas_da_chave)),
            converter_peca('\n'.join(linhas_da_fechadura))
        ))

    return saida
